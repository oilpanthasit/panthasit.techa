var express = require('express');
var router = express.Router();
var app = express();
var path = require('path');
var multer = require('multer');
var mongoose = require('mongoose');
var fs = require('fs');
var file_schema = require('../models/fileschema');
var userPath = require('../models/path');
var userdb = require('../models/user');
var groups = require('../models/group')
var shelljs = require('shelljs');
var rimraf = require('rimraf');
app.use(express.static(__dirname + "/public"));
var isAuthenticated = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    res.redirect('/login');
}

module.exports = function (passport) {

    /* GET login page. */
    router.get('/', function (req, res) {
        // Display the Login page with any flash message, if any
        //	res.render('index', { message: req.flash('message') });
        res.sendFile('index.html', {
            root: 'public'
        });

    });

    /* Handle Login POST */
    router.post('/login', passport.authenticate('login', {
        successRedirect: '/mydrive',
        failureRedirect: '/relogin',
        failureFlash: true,



    }));


    //------------------get user info-------------------------------------
    router.get('/userinfo', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var path = "";
        var foldername = "";
        userdb.findOne({
            username: username
        }, function (err, found) {
            if (err) {
                res.send('ERROR');
            } else {
                res.json(found);
            }
        })

    });

    //------------------get member info-------------------------------------
    router.get('/getmember', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var allMember = [];
        userdb.find({}, function (err, found) {
            if (err) {
                res.send('ERROR');
            } else {
                var members = found;
                members.forEach(function (member) {
                    allMember.push(member.username)
                });
                res.send(allMember);
            }
        })

    });


    //------------------get container list------------------------
    router.get('/containerlist', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var path = "";
        var foldername = "";
        userPath.findOne({
            username: username
        }, function (err, found) {
            if (err) {
                console.log(err);
                res.send('ERROR');
            } else {
                path = found.path;
                foldername = found.folder;
                file_schema.findOne({
                    name: foldername
                }, function (err, found) {
                    res.json(found);
                });
            }
        })




    });
    //--------------------------------------------------------------

    //----------------------My drive---------------------------------
    router.get('/rootcontainer', isAuthenticated, function (req, res) {
        var username = req.user.username;
        userPath.findOneAndUpdate({
            username: username
        }, {
                'path': username + '-',
                'folder': username
            }, function (err, found) {
                if (err) {
                    res.send('ERROR');
                } else {

                    res.send('success');
                }
            })

    });

    //----------------------------------------------------------------



    router.post('/updatepath/:foldername', function (req, res) {
        var foldername = req.params.foldername;
        var username = req.user.username;
        var path = "";

        userPath.findOne({
            username: username
        }, function (err, found) {
            if (err) {
                res.send('ERROR');
            } else {
                path = found.path;
                var newPath = path + foldername + "-";
                userPath.findOneAndUpdate({
                    username: username
                }, {
                        'path': newPath,
                        'folder': foldername
                    }, function (err, found) {
                        if (err) {
                            res.send('ERROR');
                        } else {
                            res.send('success');
                        }
                    })


            }
        })
    });



    /* GET Registration Page */
    router.get('/signup', function (req, res) {
        res.render('register');
    });
    router.get('/relogin', function (req, res) {
        res.sendFile('relogin.html', {
            root: 'public'
        })
    });
    /* Handle Registration POST */
    router.post('/signup', passport.authenticate('signup', {
        successRedirect: '/mydrive',
        failureRedirect: '/signup',
    }));

    /* GET Home Page */
    router.get('/mydrive', isAuthenticated, function (req, res) {

        res.sendFile('mydrive.html', {
            root: 'public'
        });

        var username = req.user.username;
    });
    //--------------------------------------------------------------

    router.post('/uploadfile', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var folder = "";

        // //--------------------upload file module---------------------------------


        var storage = multer.diskStorage({
            destination: function (req, file, callback) {
                var path = "";
                var dir = "";
                userPath.findOne({
                    username: username
                }, function (err, found) {
                    if (err) {
                        res.send('ERROR');
                    } else {
                        path = found.path;
                        folder = found.folder;
                        dir = './sitdrive_storage/' + path;
                        callback(null, dir);
                    }
                })

            },
            filename: function (req, file, callback) {
                var filename = file.originalname;
                var fileExtension = filename;
                callback(null, filename);
                // console.log(filename);

            }
        });
        var upload = multer({
            storage: storage
        }).single('file');
        // //------------------------------------------------------------------
        upload(req, res, function (err) {
            if (err) {
                res.end("Error uploading file.");
            }
            var filename = req.file.filename;
            var filesize = req.file.size;

            file_schema.findOne({ name: folder }, function (err, found) {
                if (err) {
                    res.send('ERROR');
                } else {
                    found.file.push({
                        filename: filename,
                        size: filesize
                    });
                    found.save();
                    res.redirect('/mydrive');
                }
            })

            userdb.findOne({
                username: username
            }, function (err, doc) {
                if (err) {
                    res.send('ERROR');
                } else {
                    var used = doc.used;
                    var updateUsed = used + filesize;
                    doc.used = updateUsed;
                    doc.save();
                }
            })
        });
    });
    // //------------------------------------------------------------------


    //-------------------delete file--------------------------------
    router.delete('/containerlist/:fileid/:filename/:filesize', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var filename = req.params.filename;
        var fileid = req.params.fileid;
        var filesize = req.params.filesize;
        var path = "";
        var foldername = "";
        userPath.findOne({
            username: username
        }, function (err, found) {
            if (err) {
                console.log(err);
                res.send('ERROR');
            } else {
                path = found.path;
                foldername = found.folder;
                file_schema.update({
                    $and: [{
                        name: foldername
                    }, {
                        path: path
                    }]
                }, {
                        $pull: {
                            "file": {
                                filename: filename
                            }
                        }
                    }, function (err) {
                        if (err) {
                            console.log(err);
                        } else {
                            var removefile = './sitdrive_storage/' + path + '/' + filename;
                            fs.unlinkSync(removefile);
                            userdb.findOne({
                                username: username
                            }, function (err, doc) {
                                if (err) {
                                    console.log(err);
                                    res.send('ERROR');
                                } else {

                                    var used = doc.used;
                                    var updateUsed = used - filesize;
                                    doc.used = updateUsed;
                                    doc.save();
                                    // console.log("filesize"+filesize)
                                    // console.log(updateUsed);
                                    // console.log(doc)
                                    userdb.update({
                                        $and: [{
                                            "sharedfile.filename": filename
                                        }, {
                                            "sharedfile.owner": username
                                        }]
                                    }, {
                                            $pull: {
                                                "sharedfile": {
                                                    filename: filename
                                                }
                                            }
                                        }, function (err) {
                                            if (err) {
                                                console.log(err);
                                            } else { }
                                        })
                                }
                            })
                            return res.send("success");
                        }
                    })

            }
        })


    });
    //----------------------------------------------------------------

    router.post('/creategroup/', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var newgroupname = String(req.body.groupname);
        res.sendFile('newgroup.html', {
            root: 'public'
        });

    });
    //----------------------------Create folder routing-----------------
    router.post('/createfolder/', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var foldername = String(req.body.name);

        var path = "";
        var pathfoldername = "";
        userPath.findOne({
            username: username
        }, function (err, found) {
            if (err) {
                res.send('ERROR');
            } else {
                path = found.path;
                pathfoldername = found.folder;
                // console.log("PATH ::" + path);
                file_schema.findOne({
                    name: pathfoldername
                }, function (err, found) {
                    if (err) {
                        res.send('ERROR');
                    } else {
                        found.childfolder.push({
                            foldername: foldername
                        });
                        found.save();

                        var newFolder = new file_schema();

                        newFolder.name = foldername;
                        newFolder.parentfolder = username;
                        newFolder.owner = username;
                        newFolder.path = path;
                        newFolder.save(function (err) {
                            if (err) {
                                console.log('Error in Saving Newfolder: ' + err);
                                throw err;
                            }
                            shelljs.mkdir('-p', './sitdrive_storage/' + path + foldername + '-');
                            res.redirect('/mydrive');
                        });
                    }
                })
            }
        })

    });


    //------------------------------download file routing--------------------
    router.get('/download/:file(*)', isAuthenticated, function (req, res, next) {
        var username = req.user.username;
        var file = req.params.file;
        var path = "";
        userPath.findOne({
            username: username
        }, function (err, found) {
            if (err) {
                res.send('ERROR');
            } else {
                path = found.path;
                var downloadPath = './sitdrive_storage/' + path + '/' + file
                res.download(downloadPath);
            }
        })


    });
    //------------------------------------------------------------------------



    //-------------------delete folder--------------------------------
    router.delete('/folderlist/:folderid/:foldername', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var foldername = req.params.foldername;
        var folderid = req.params.folderid;
        var pathfolder = "";
        var path = "";
        userPath.findOne({
            username: username
        }, function (err, found) {
            if (err) {
                res.send('ERROR');
            } else {
                path = found.path;
                pathfolder = found.folder;
                // console.log("PATH ::" + path);

                //-----------------------------------------------------------------------------
                file_schema.update({
                    name: pathfolder
                }, {
                        $pull: {
                            "childfolder": {
                                _id: folderid
                            }
                        }
                    }, function (err) {
                        if (err) { } else {

                        }
                    })
                var totalFileSize = 0;
                file_schema.findOneAndRemove({
                    $and: [{
                        name: foldername
                    }, {
                        path: path
                    }]
                }, function (err, found) {
                    if (err) {
                        console.log(err);
                    } else {
                        var fileList = found.file;
                        fileList.forEach(function (file) {
                            totalFileSize += file.size;
                        });
                    }
                    userdb.findOne({
                        username: username
                    }, function (err, doc) {
                        if (err) {
                            console.log(err);
                            res.send('ERROR');
                        } else {
                            var used = doc.used;
                            var updateUsed = used - totalFileSize;
                            doc.used = updateUsed;
                            doc.save();
                        }
                    })
                    userdb.update({
                        $and: [{
                            "sharedfolder.foldername": foldername
                        }, {
                            "sharedfolder.owner": username
                        }]
                    }, {
                            $pull: {
                                "sharedfolder": {
                                    foldername: foldername
                                }
                            }
                        }, function (err) {
                            if (err) {
                                console.log(err);
                            } else { }
                        })

                    rimraf('./sitdrive_storage/' + path + foldername + '-', function (err) {
                        if (err) {
                            throw err;
                        }
                        // done
                        return res.send("success");
                    });
                });
            }
        })




    });
    //----------------------------------------------------------------

    //------------------------------Share file------------------------------
    router.post('/sharefile/:filesize/:filename/:data', isAuthenticated, function (req, res) {

        var username = req.user.username;
        var filename = req.params.filename;
        var filesize = req.params.filesize;
        var data = req.params.data;
        var members = JSON.parse(data);
        var owner = username;
        var path = "";
        var foldername = "";

        members.forEach(function (member) {
            userPath.findOne({
                username: username
            }, function (err, found) {
                if (err) {
                    console.log(err);
                    res.send('ERROR');
                } else {
                    path = found.path;
                    foldername = found.folder;
                    userdb.findOne({
                        username: member.name
                    }, function (err, doc) {
                        if (err) {
                            console.log(err);
                            res.send('ERROR');
                        } else {

                            doc.sharedfile.push({
                                filename: filename,
                                path: path,
                                owner: owner,
                                size: filesize
                            });

                            doc.save();
                            // console.log("USER INFO" + doc);

                        }
                    })
                }
            })
        });

    });

    //----------------------------------------------------------------------

    //----------------------Share folder------------------------------------
    router.post('/sharefolder/:foldername/:data', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var foldername = req.params.foldername;
        var data = req.params.data;
        var members = JSON.parse(data);
        var owner = username;
        var path = "";

        members.forEach(function (member) {
            userPath.findOne({
                username: username
            }, function (err, found) {
                if (err) {
                    console.log(err);
                    res.send('ERROR');
                } else {
                    path = found.path;
                    userdb.findOne({
                        username: member.name
                    }, function (err, doc) {
                        if (err) {
                            console.log(err);
                            res.send('ERROR');
                        } else {

                            doc.sharedfolder.push({
                                foldername: foldername,
                                path: path,
                                owner: owner
                            });

                            doc.save();

                        }
                    })
                }
            })
        });

    });
    //---------------------------------------------------------------------


    //------------------------------Sharewith------------------------------------

    router.get('/sharewithme', function (req, res) {
        res.sendFile('sharewithmepage.html', {
            root: 'public'
        });

    });

    //------------------get sharedcontainer list------------------------
    router.get('/sharecontainerlist', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var container = [];
        userdb.findOne({
            username: username
        }, function (err, found) {

            if (err) {
                console.log(err);
                res.send('ERROR');
            } else {
                var sharefiles = found.sharedfile;
                var sharefolders = found.sharedfolder;
                res.send({
                    sharefile: sharefiles,
                    sharefolder: sharefolders
                });
            }
        })
        userPath.findOneAndUpdate({
            username: username
        }, {
                'sharedpath': username + '-',
                'shareddir': username + "-",
                'sharedfolder': username
            }, function (err, found) {
                if (err) {
                    console.log(err);
                    res.send('ERROR');
                } else {

                }
            })
    });

    //-----------------------------get Shared folder content route---------------------
    router.get('/sharedfoldercontent/:foldername/:path', isAuthenticated, function (req, res, next) {
        var username = req.user.username;
        var foldername = req.params.foldername;
        var path = req.params.path;

        file_schema.findOne({
            $and: [{
                name: foldername
            }, {
                path: path
            }]
        }, function (err, doc) {
            if (err) {
                console.log(err);
            } else {
                // console.log(doc);
                res.send(doc);
                // res.send({data:doc,newPath:newPath});
            }
        });
        userPath.findOneAndUpdate({
            username: username
        }, {
                'sharedpath': path,
                'shareddir': path + foldername + "-",
                'sharedfolder': foldername
            }, function (err, found) {
                if (err) {
                    console.log(err);
                    res.send('ERROR');
                } else { }
            })

    });



    //-----------------------------get sub folder content route---------------------
    router.get('/subfoldercontent/:foldername/:path', isAuthenticated, function (req, res, next) {
        var username = req.user.username;
        var foldername = req.params.foldername;
        var path = req.params.path;

        file_schema.findOne({
            $and: [{
                name: foldername
            }, {
                path: path
            }]
        }, function (err, doc) {
            if (err) {
                console.log(err);
            } else {
                // console.log(doc);
                res.send(doc);

                // res.send({data:doc,newPath:newPath});
            }
        });
        userPath.findOneAndUpdate({
            username: username
        }, {
                'sharedpath': path,
                'shareddir': path + foldername + "-",
                'sharedfolder': foldername
            }, function (err, found) {
                if (err) {
                    console.log(err);
                    res.send('ERROR');
                } else { }
            })

    });




    //------------------------------download shared file routing--------------------
    router.get('/downloadshare/:file(*)/:path', isAuthenticated, function (req, res, next) {
        var username = req.user.username;
        var file = req.params.file;
        var path = req.params.path;
        var downloadPath = './sitdrive_storage/' + path + '/' + file
        res.download(downloadPath);

    });
    //--------------------------------------------------------------


    //-------------------Upload file sharewithme page ----------------------
    router.post('/shareuploadfile', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var foldername = "";
        //--------------------upload file module---------------------------------
        var path = "";

        var storage = multer.diskStorage({
            destination: function (req, file, callback) {
                var dirpath = "";
                var dir = "";
                userPath.findOne({
                    username: username
                }, function (err, found) {
                    if (err) {
                        res.send('ERROR');
                    } else {
                        path = found.sharedpath;
                        dirpath = found.shareddir;
                        dir = './sitdrive_storage/' + dirpath;
                        foldername = found.sharedfolder
                        path = found.sharedpath;
                        callback(null, dir);
                    }
                })

            },
            filename: function (req, file, callback) {
                var filename = file.originalname;
                var fileExtension = filename;
                callback(null, filename);
                // console.log(filename);

            }
        });
        var upload = multer({
            storage: storage
        }).single('file');
        // //------------------------------------------------------------------
        upload(req, res, function (err) {
            if (err) {
                res.end("Error uploading file.");
            }
            var filename = req.file.filename;
            var filesize = req.file.size;
            file_schema.findOne({
                $and: [{
                    name: foldername
                }, {
                    path: path
                }]
            }, function (err, found) {
                if (err) {
                    res.send('ERROR');
                } else {
                    found.file.push({
                        filename: filename,
                        size: filesize
                    });
                    found.save();
                    res.redirect('/sharewithme');
                }
            })

            userdb.findOne({
                username: username
            }, function (err, doc) {
                if (err) {
                    res.send('ERROR');
                } else {
                    var used = doc.used;
                    var updateUsed = used + filesize;
                    doc.used = updateUsed;
                    doc.save();
                }
            })
        });
    });

    //----------------------------Create folder routing-----------------
    router.post('/sharedcreatefolder/', isAuthenticated, function (req, res) {
        var username = req.user.username;
        var foldername = String(req.body.name);

        var sharedpath = "";
        var sharedfolder = "";
        userPath.findOne({
            username: username
        }, function (err, found) {
            if (err) {
                res.send('ERROR');
            } else {
                sharedpath = found.sharedpath;
                sharedfolder = found.sharedfolder;
                var shareddir = found.shareddir;
                file_schema.findOne({
                    $and: [{
                        name: sharedfolder
                    }, {
                        path: sharedpath
                    }]
                }, function (err, found) {
                    if (err) {
                        res.send('ERROR');
                    } else {
                        found.childfolder.push({
                            foldername: foldername
                        });
                        found.save();

                        var newFolder = new file_schema();

                        newFolder.name = foldername;
                        newFolder.parentfolder = username;
                        newFolder.owner = username;
                        newFolder.path = shareddir;
                        newFolder.save(function (err) {
                            if (err) {
                                throw err;
                            }
                            shelljs.mkdir('-p', './sitdrive_storage/' + shareddir + foldername + '-');
                            res.redirect('/sharewithme');
                        });
                    }
                })
            }
        })






    });



    /* Handle Logout */
    router.get('/signout', isAuthenticated, function (req, res) {

        var username = req.user.username;
        userPath.findOneAndUpdate({
            username: username
        }, {
                'path': username + '-'
            }, function (err, found) {
                if (err) {
                    res.send('ERROR');
                } else {
                    req.logout();
                    return res.redirect('/');

                }
            })

    });


    //------------------------------download sub file routing--------------------
    router.get('/downloadsubfile/:file(*)', isAuthenticated, function (req, res, next) {
        var username = req.user.username;
        var file = req.params.file;
        var downloadPath = './sitdrive_storage/' + path + '/' + file
        res.download(downloadPath);



    });
    //--------------------------------------------------------------


    //--------------------------My group route------------------------
    router.get('/mygroups', function (req, res) {
        res.sendFile('mygroups.html', {
            root: 'public'
        });
    });

    // ----------------------------------edit group route-------------------------------------

    router.get('/editgroup', function (req, res) {
        res.sendFile('editgroup.html', {
            root: 'public'
        });

    });


    return router;
}
