var mongoose = require('mongoose');
module.exports = mongoose.model('userpaths', {
    "username": { type: String },
    "folder": { type: String },
    "path": { type: String },
    "shareddir": { type: String },
    "sharedpath": { type: String },
    "sharedfolder": { type: String }
    

});