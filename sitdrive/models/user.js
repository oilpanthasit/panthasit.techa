
var mongoose = require('mongoose');

module.exports = mongoose.model('User', {
	"username": String,
	"password": String,
	"sharedfolder": [
		{
			"foldername": { type: String },
			"path": { type: String },
			"owner": { type: String },
			"upload": { type: String },
			"delete": { type: String }
		}
	],
	"sharedfile": [
		{
			"filename": { type: String },
			"path": { type: String },
			"owner": { type: String },
			"size": { type: String }
		}
	]
	,
	"quota": { type: Number },
	"used": { type: Number }
	//104857600 bytes = 100mb
});