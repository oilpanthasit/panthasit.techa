var mongoose = require('mongoose');
module.exports = mongoose.model('file_schema', {
    "name": { type: String},
    "owner": {type: String},
    "type":{type:String},
    "path": {type: String},
    "childfolder": [
        { "foldername": { type: String } }
    ],
    "file": [
        { "filename": { type: String }, 
        "size": { type: Number} }
        
    ],
    "parentfolder":{ type: String}
});