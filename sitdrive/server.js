var express = require('express');
var app = express();
var mongojs = require('mongojs');
var bodyParser = require('body-parser');
var dbConfig = require('./db');
var path = require('path');
var multer = require('multer');
var fs = require('fs');
var mongoose = require('mongoose');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var favicon = require('static-favicon');
var router = express.Router();


app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(favicon(__dirname + './public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.urlencoded());
app.use(cookieParser());
mongoose.connect(dbConfig.url);
app.set('views', path.join(__dirname, 'public'));
app.set('view engine', 'jade');
//Passport
var passport = require('passport');
var expressSession = require('express-session');

//TODO - Why Do we need this key ?
app.use(expressSession({
    secret: 'mySecretKey'
}));
app.use(passport.initialize());
app.use(passport.session());

var flash = require('connect-flash');
app.use(flash());


// Initialize Passport
var initPassport = require('./passport/init');
initPassport(passport);
// init routes
var routes = require('./routes/index')(passport);
app.use('/', routes);
/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    res.redirect('/');
    next(err);
});
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
    });
}

module.exports = app;

app.listen(3000);
console.log("Server running on port 3000");
