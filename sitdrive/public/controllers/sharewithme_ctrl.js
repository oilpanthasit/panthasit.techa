function AppCtrl($scope, $http) {

    $scope.path = "";

    var refresh = function () {
        $http.get('/sharecontainerlist').success(function (response) {
            $scope.sharedfilelist = response.sharefile;
            $scope.sharefile = "";

            $scope.sharedfolderlist = response.sharefolder;
            $scope.sharedfolder = "";
        });

    };

    $scope.sharewithme = function () {
         $http.get('/sharecontainerlist').success(function (response) {
            $scope.sharedfilelist = response.sharefile;
            $scope.sharefile = "";

            $scope.sharedfolderlist = response.sharefolder;
            $scope.sharedfolder = "";

             $scope.subfilelist = "";
             $scope.subfolderlist = "";
        });

    }

    refresh();


    $scope.getSharedfolderContent = function (foldername, path) {

        $http.get('/sharedfoldercontent' + '/' + foldername + '/' + path).success(function (response) {
            var childfolder = response.childfolder;
            var filelist = response.file;
            $scope.sharedfilelist = "";


            $scope.sharedfolderlist = "";


            $scope.subfolderlist = childfolder;
            $scope.subfolder = "";

            $scope.subfilelist = filelist;
            $scope.subfile = "";
            $scope.path = path + foldername + '-';
            subfolderPath = path + foldername + "-";
        });
    };


    $scope.getSubfolderContent = function (foldername) {


        $http.get('/subfoldercontent' + '/' + foldername + '/' + subfolderPath).success(function (response) {
            console.log(response);
            var childfolder = response.childfolder;
            var filelist = response.file;

            $scope.subfolderlist = childfolder;
            $scope.subfolder = "";

            $scope.subfilelist = filelist;
            $scope.subfile = "";

            subfolderPath = subfolderPath + foldername + "-";
            $scope.path = subfolderPath;

        });
    };

    var getUser = function () {



        $http.get('/userinfo').success(function (response) {
            $scope.username = response.username;
            $scope.used = response.used;
            $scope.quota = response.quota;
            console.log(response);
        });

    };
    getUser();




}
