function AppCtrl($scope, $http) {


    $scope.updatePath = function(foldername) {
        $http.post('/updatepath/' + foldername).success(function() {
            refresh();
        });

    };
    var systemMember = "";
    var getMember = function() {
        $http.get('/getmember').success(function(response) {
            console.log(response);
            systemMember = response;
        });
    }
    getMember();

    $scope.myDrive = function() {
        $http.get('/rootcontainer').success(function() {

            refresh();
        });
    }


    var refresh = function() {



        $http.get('/containerlist').success(function(response) {
            $scope.containerlist = response.file;
            $scope.file = "";
            $scope.folderlist = response.childfolder;
            $scope.folder = "";
        });
    };

    refresh();

    var getUser = function() {
        $http.get('/userinfo').success(function(response) {
            $scope.username = response.username;
            $scope.used = response.used;
            $scope.quota = response.quota;
        });

    };
    getUser();


    //--------------------delete file-------------------------------
    $scope.remove = function(file) {
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    $http.delete('/containerlist/' + file._id + '/' + file.filename + '/' + file.size).success(function(response) {
                        swal("Deleted!", file.filename + " has been deleted.", "success");
                        refresh();
                        getUser();
                    })

                }
            });
    };
    //--------------------------------------------------------------------------------------------------------------------

    //----------------------------delete folder-------------------------------------------------------------------------------
    $scope.removeFolder = function(folder) {
        swal({
                title: "Warning!!",
                text: "All files in \"" + folder.foldername + "\" will be deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    $http.delete('/folderlist/' + folder._id + '/' + folder.foldername + '/').success(function(response) {
                        swal("Deleted!", folder.foldername + " has been deleted.", "success");
                        refresh();
                        getUser();
                    })
                    getUser();
                    refresh();
                }
            });
    };

    //-----------------------------SHAREFILE-----------------------

    $scope.varfilename = "";
    $scope.varfilesize = "";

    $scope.clickfilename = function(filename, filesize) {
        $scope.varfilename = filename;
        $scope.varfilesize = filesize;

    };

    $scope.member = [];
    $scope.update = function(user) {

        data = {
                name: user.name
            }
        var memberbool = false;
        for (i = 0; i < systemMember.length; i++) {
            if (systemMember[i] === user.name) {
                memberbool = true;
            }
        }
        if (memberbool) {
            $scope.member.push(data)
        }else {
            alert('This member is not in sitDrive');
            user.name = "";
        }
    };


    $scope.reset = function() {
        $scope.user = angular.copy($scope.member);
        $scope.member = [];
    }


    $scope.sharefile = function(data) {
            var groupData = JSON.stringify(data);

            $http.post('/sharefile/' + $scope.varfilesize + '/' + $scope.varfilename + '/' + groupData).success(function(response) {

                refresh();

            })
            if ($scope.member.length == 0) {
                alert("Plese insert at least one persone to share");
            } else {
                swal("Share completed!")
                window.location = '/mydrive';
            }

            refresh();
        }
        //------------------------------------------------------------------------------------




    // --------------------------Share folder--------------------------------------------
    $scope.varfoldername = "";


    $scope.clickfoldername = function(foldername) {
        $scope.varfoldername = foldername;
    };


    $scope.foldermember = [];

    $scope.folderUpdate = function(user) {

        data = {
            name: user.name
        }
        var memberbool = false;
        for (i = 0; i < systemMember.length; i++) {
            if (systemMember[i] === user.name) {
                memberbool = true;
            }
        }
        if (memberbool) {
            $scope.foldermember.push(data)
        }else {
            alert('This member is not in sitDrive');
            user.name = "";
        }
    };


    $scope.folderReset = function() {
        $scope.user = angular.copy($scope.foldermember);
        $scope.foldermember = [];
    }


    $scope.sharefolder = function(data) {
        var groupData = JSON.stringify(data);
        $http.post('/sharefolder/' + $scope.varfoldername + '/' + groupData).success(function(response) {
            refresh();

        })
        if ($scope.foldermember.length == 0) {
            alert("Plese insert at least one persone to share");
        } else {
            swal("Share completed!")
            window.location = '/mydrive';
        }

        refresh();


    }


    $('#myFile').bind('change', function() {
        if ($scope.used + this.files[0].size > $scope.quota) {
            alert("You've reached the quota");
            window.location = '/mydrive'
        }
        $scope.thisfilesize = this.files[0].size;

    });


}
