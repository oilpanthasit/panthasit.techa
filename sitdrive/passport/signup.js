var LocalStrategy = require('passport-local').Strategy;
var User = require('../models/user');
var userPath = require('../models/path');
var bCrypt = require('bcrypt-nodejs');
var shelljs = require('shelljs');
var mkdirp = require('mkdirp');
var fileschema = require('../models/fileschema');

module.exports = function (passport) {

    passport.use('signup', new LocalStrategy({
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
        function (req, username, password, done) {

            findOrCreateUser = function () {
                // find a user in Mongo with provided username
                User.findOne({
                    'username': username
                }, function (err, user) {
                    // In case of any error, return using the done method
                    if (err) {
                        // console.log('Error in SignUp: ' + err);
                        return done(err);
                    }
                    // already exists
                    if (user) {
                        // console.log('User already exists with username: ' + username);
                        return done(null, false, req.flash('message', 'User Already Activated'));
                    } else {
                        // if there is no user with that email
                        // create the user
                        var newUser = new User();

                        // set the user's local credentials
                        newUser.username = username;
                        newUser.password = createHash(password);
                        newUser.quota = 1073741824; //1GB
                        newUser.used = 0.00;
                        // newUser.type = 'user';
                        // newUser.memberlist = [];

                        //create user folder
                        var userfolder = newUser.username;

                        // save the user
                        newUser.save(function (err) {
                            if (err) {
                                console.log('Error in Saving user: ' + err);
                                throw err;
                            }

                            shelljs.mkdir('-p', './sitdrive_storage/' + newUser.username + '-');
                            return done(null, newUser);
                        });

                        //initialize schema
                        var fileschemas = new fileschema();
                        fileschemas.name = username;
                        fileschemas.owner = username;
                        fileschemas.parentfolder = "root";
                        fileschemas.path = username + "-";
                        fileschemas.save(function (err) {
                            if (err) {

                                throw err;
                            }
                            return done(null, fileschemas);
                        });


                        var newUserPath = new userPath();
                        newUserPath.username = username;
                        newUserPath.path = username + '-';
                        newUserPath.folder = username;
                        newUserPath.sharedpath = username + '-';
                        newUserPath.shareddir = username + '-';
                        newUserPath.sharedfolder = username;
                        newUserPath.save(function (err) {
                            if (err) {
                                console.log('Error in Saving Path: ' + err);
                                throw err;
                            }
                            return console.log("save path");
                        });
                    }
                });
            };
            // Delay the execution of findOrCreateUser and execute the method
            // in the next tick of the event loop
            process.nextTick(findOrCreateUser);
        }));

    // Generates hash using bCrypt
    var createHash = function (password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

}
